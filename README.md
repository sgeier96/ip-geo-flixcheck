# 🚀 IP-Geo-Flixcheck

## 🖼️ Showcase-GIF

![Showcase-Gif](showcase.gif)

## 📜 Eine kurze Einleitung
Aufbauend auf der Vorgabe, habe ich die Webapp in devcontainern entwickelt.
Frontend und Backend sind dabei eigene Container und liegen in demselben Netzwerk. 
Für das Frontend habe ich noch Chakra UI  dazu gezogen, nachdem ich mich kurz an Rebass probiert habe.
Außerdem habe ich das Ergebnis der ip-api-Anfrage durch eine weitere Api gejagt, mit der ich vorher nichts zu tun hatte. 
Mit alledem wollte ich folgende Punkte zeigen und hoffe sehr, dass es auch funktioniert hat 😃:
- In der Lage, mit Docker zu arbeiten
- In der Lage, neue Frameworks schnell (wenn auch minimal) zur Funktion zu bringen – ChakraUI habe ich vorher nie genutzt, bisher nur MaterialUI
- In der Lage, auch komplexere APIs bzw. Komponenten zu verwenden als ip-api. Mapbox wurde zur Generierung der interaktiven Karte genutzt und auch das hab ich heute zum ersten Mal genutzt.

Offen gesagt konnte ich das bauen und starten bisher nur ein mal auf meiner lokalen Maschine testen – gerne können wir da demnächst drüber quatschen wieso das so gekommen ist, ich hoffe aber sehr, dass aber alles funktioniert. Ansonsten gerne anrufen oder notfalls als devcontainer starten.
Ich hoffe natürlich sehr, dass meine Lösung überzeugt – so oder so würde ich mich aber gerne auch zu Feedback zu meinem Code freuen! :) 

Lieben Gruß
Stefan 👋

## 🐳 Bauanleitung
Voraussetzung hierfür: earthly 🌍

- Docker-Netzwerk erstellen: `docker network create -d bridge ip-geo-network` 
- Bauen der images (im root des repos): `earthly +production`
- Starten der Container (im root des repos): `docker compose up`
- Fertig ✅
- Frontend nun erreichbar unter: `localhost:3000`

import { Router } from "express";
import { getGeo } from '../controllers/ip-geo.controller'

const router = Router();
router.post('/', getGeo);

export default router;
import express, { Request, Response, RequestHandler } from 'express';
import axios from "axios";

const isIP = (query: string): boolean => {
    const ipRegex = /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/;
    return ipRegex.test(query);
}

export const getGeo: RequestHandler = async (req, res, next) => {
    if (!isIP(req.body["ip"])) {
        res.status(400).json({ "message": `Invalid input. Input must be a correctly formatted ip adress. Gotten: ${req.body["ip"]}` })
        return;
    }
    axios.get(`http://ip-api.com/json/${req.body["ip"]}`)
        .then((response) => {
            const geoData: {} = { "response": response.data };
            res.json({ message: 'great success!', geo: geoData })
        })
        .catch((error) => {
            res.status(error.status).json({ "message": error.message });
        });
};
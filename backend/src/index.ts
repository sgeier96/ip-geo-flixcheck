import express, { Request, Response, NextFunction } from 'express';
import ipGeoRoutes from './api/routes/ip-geo.routes';
import { json } from 'body-parser';
import cors from 'cors';

const app = express();
app.use(cors());
app.use(json());
app.use('/getGeo', ipGeoRoutes);
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({ message: err.message });
})

app.listen(5000);

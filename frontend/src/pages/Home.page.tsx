import { FC } from "react";
import IpGeoComponent from "../features/components/ipGeo.component";

const HomePage: FC = () => {

    return (<IpGeoComponent />);
}

export default HomePage;
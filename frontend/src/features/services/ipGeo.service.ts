import axios from 'axios';
import { IpGeoResponse } from '../models/ipGeo.interface';

axios.interceptors.request.use(
    (config) => {
        console.log('Request:', config);
        return config;
    },
    (error) => {
        console.error('Request error:', error);
        return Promise.reject(error);
    }
);
axios.interceptors.response.use(
    (response) => {
        console.log('Response:', response);
        return response;
    },
    (error) => {
        console.error('Response error:', error);
        return Promise.reject(error);
    }
);

const isIP = (query: string): boolean => {
    const ipRegex = /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/;
    return ipRegex.test(query);
}

export const getGeo = async (ipInput: string): Promise<IpGeoResponse | null> => {
    if (!isIP(ipInput)) return null;
    try {
        const response = await axios.post(`http://localhost:4999/getGeo/`, { "ip": ipInput });
        return response.data as IpGeoResponse;
    } catch (error) {
        console.error('Error: ' + error);
        return null;
    }
}
import React, { FC } from "react";
import { getGeo } from "../services/ipGeo.service";
import { publicIpv4 } from "public-ip";
import Lottie from "lottie-react";
import worldDotsAnimation from "../../animations/world_dots.json";
import {
  FormControl,
  FormErrorMessage,
  Input,
  Flex,
  Center,
  Stack,
  Button,
  Heading,
  InputRightElement,
  InputGroup,
  Box,
  Icon,
} from "@chakra-ui/react";
import { Search2Icon, SpinnerIcon, ViewIcon } from "@chakra-ui/icons";
import MapModalComponent from "./mapModal.component";
import { IpGeoPack, initialGeoPack } from "../models/ipGeo.interface";
import { useDisclosure } from "@chakra-ui/react";

const IpGeoComponent: FC = () => {
  const [ipInput, setIpInput] = React.useState<string>("");
  const [loading, setLoading] = React.useState<boolean>(false);
  const [ipLoads, setIpLoads] = React.useState<boolean>(false);
  const [isError, setIsError] = React.useState<boolean>(false);
  const [geoData, setGeoData] = React.useState<IpGeoPack>(initialGeoPack);
  const { isOpen, onOpen, onClose } = useDisclosure();

  React.useEffect(() => {
    setIpLoads(true);
    publicIpv4()
      .then((resolve) => setIpInput(resolve))
      .finally(() => setIpLoads(false));
  }, []);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setIpInput(e.target.value);
  };

  const handleSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setLoading(true);
    const resultPromise = Promise.allSettled([
      getGeo(ipInput),
      new Promise((resolve) => setTimeout(resolve, 3000)),
    ]);
    const results = await resultPromise;
    const result =
      results[0].status === "fulfilled" ? results[0].value : undefined;
    setLoading(false);
    if (result) {
      setGeoData(result["geo"]["response"]);
      onOpen();
    } else {
      setIsError(true);
    }
  };

  return (
    <>
      <MapModalComponent opened={isOpen} handleClose={onClose} {...geoData} />
      <Flex justify="center" align="center" height="100vh">
        <Center>
          <Box width={500}>
            {loading ? (
              <Lottie animationData={worldDotsAnimation} loop />
            ) : (
              <FormControl isInvalid={isError} width={500}>
                <Stack direction="column" spacing={4}>
                  <Heading color={"rgba(175, 47, 47, 0.15)"}>
                    Spyglass
                    <Icon color={"rgba(175, 47, 47, 0.15)"} ml={2}>
                      <ViewIcon />
                    </Icon>
                  </Heading>
                  <InputGroup size="lg">
                    <Input
                      value={ipInput}
                      onChange={handleInputChange}
                      size="lg"
                    />
                    <InputRightElement width="4.5rem">
                      <Button
                        size="sm"
                        onClick={handleSubmit}
                        rightIcon={ipLoads ? <SpinnerIcon /> : <Search2Icon />}
                        variant="outline"
                        disabled={!loading}
                      >
                        Go
                      </Button>
                    </InputRightElement>
                  </InputGroup>
                  {isError ? (
                    <FormErrorMessage>Invalid IP!</FormErrorMessage>
                  ) : undefined}
                </Stack>
              </FormControl>
            )}
          </Box>
        </Center>
      </Flex>
    </>
  );
};

export default IpGeoComponent;

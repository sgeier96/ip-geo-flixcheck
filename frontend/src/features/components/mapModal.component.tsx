import MapComponent from "./map.component";
import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import { IpGeoPack } from "../models/ipGeo.interface";

interface MapModalProps extends IpGeoPack {
  opened: boolean;
  handleClose: () => void;
}

const MapModalComponent = ({
  opened,
  handleClose,
  ...props
}: MapModalProps) => {
  return (
    <Modal
      closeOnOverlayClick={false}
      isOpen={opened}
      onClose={handleClose}
      isCentered
      motionPreset="slideInBottom"
      size="2xl"
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Our spies have been successful!</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={6}>
          <MapComponent {...props}/>
        </ModalBody>
        <ModalFooter>
          <Button onClick={handleClose}>Aha!</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default MapModalComponent;

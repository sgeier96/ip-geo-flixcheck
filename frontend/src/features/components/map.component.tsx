/** Due to a bug with mapboxgl that results in breaking the map in production, some adjustment had to be made.
 * See: https://github.com/visgl/react-map-gl/issues/1266
 * Or/And: https://github.com/alex3165/react-mapbox-gl/issues/938
 */
import React from "react";
import { Box } from "@chakra-ui/react";
import { IpGeoPack } from "../models/ipGeo.interface";
/* eslint-disable import/no-webpack-loader-syntax */
import mapboxgl from 'mapbox-gl';
// @ts-ignore
mapboxgl.workerClass = require('worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker').default;
mapboxgl.accessToken = "pk.eyJ1IjoiZ2VpZXJzIiwiYSI6ImNsZndod2wzNDBoZ2Ezb3M2aTdkYmViaXgifQ.DWduoQalX4QAW2smiovFkA";

const MapComponent = ({ lon, lat, city, country, ...props }: IpGeoPack) => {
  const mapContainer = React.useRef<HTMLDivElement>(null);
  const map = React.useRef<mapboxgl.Map | null>(null);
  const [longitute,] = React.useState<number>(lon);
  const [latitude,] = React.useState<number>(lat);

  React.useEffect(() => {
    if (map.current) return;
    map.current = new mapboxgl.Map({
      container: mapContainer.current!,
      style: "mapbox://styles/mapbox/streets-v12",
      center: [longitute, latitude],
      zoom: 15
    });
    new mapboxgl.Marker().setLngLat([longitute, latitude]).addTo(map.current);
  });
  
  return (
    <Box>
      <Box position="absolute">
        <Box className="sidebar">
          Longitude: {longitute} | Latitude: {latitude} | City: {city} | Country: {country}
        </Box>
      </Box>
      <Box ref={mapContainer} className="map-container" />
    </Box>
  );
};

export default MapComponent;

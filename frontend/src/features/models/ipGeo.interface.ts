export interface IpGeoPack {
    query: string,
    status: string,
    continent?: string,
    continentCode?: string,
    country: string,
    region: string,
    regionName: string,
    city: string,
    district?: string,
    zip: string,
    lat: number,
    lon: number,
    timezone: string,
    offset?: number,
    currency?: string,
    isp: string,
    org: string,
    as: string,
    asname?: string,
    mobile?: boolean,
    proxy?: boolean,
    hosting?: boolean
}

export const initialGeoPack: IpGeoPack = {
    query: "",
    status: "inital",
    country: "",
    region: "",
    regionName: "",
    city: "",
    zip: "",
    lat: -1,
    lon: -1,
    timezone: "",
    isp: "",
    org: "",
    as: ""
}

export interface IpGeoResponse {
    message: string,
    geo: {
        response: IpGeoPack
    }
}
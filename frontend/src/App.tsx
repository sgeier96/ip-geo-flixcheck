import {BrowserRouter as Router, Navigate, Route, Routes} from 'react-router-dom';
import HomePage from './pages/Home.page'

const App = () => {
  return (
    <div style={{height: '100vh', width: '100vw', backgroundColor: '#f5f5f5'}}>
      <Router>
        <Routes>
          <Route path='/ipGeo' element={<HomePage />} />
          <Route path='*' element={<Navigate to='/ipGeo'/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
